package misato.codeTest;

import java.util.*;

public class OperandsMain {

    public static void main(String args[]) {
        String str = " ( 1 + 2 ) + 3 ";
        double result = calculate(str);
        System.out.println(result);
    }

    public static double calculate(String str) {
        // split all the numbers, operators and brackets in a string array
        String[] splitNumbersAndOperands = str.split(" ");
        // Convert string array into a list
        List<String> numberAndOperatorsList = new ArrayList<String>();
        Collections.addAll(numberAndOperatorsList, splitNumbersAndOperands);

        double result = 0;

        // This part is to capture all the set with brackets
        // eg: if ( ( 1 + 3 ) - 1 )
        // In this case, it should capture 2 set of open and close parenthesis start and end position
        Map bracketsSet = new TreeMap<String, String>(Collections.reverseOrder());
        List<String> copyNumberAndOperatorsList = new ArrayList<>(numberAndOperatorsList);
        for (int index = 0; index < copyNumberAndOperatorsList.size(); index++) {
            if (copyNumberAndOperatorsList.get(index).contains("(")) {
                for (int closeBracketIndex = copyNumberAndOperatorsList.size() - 1; closeBracketIndex > 0; closeBracketIndex--) {
                    if (copyNumberAndOperatorsList.get(closeBracketIndex).contains(")")) {
                        bracketsSet.put(index, closeBracketIndex);
                        copyNumberAndOperatorsList.remove(closeBracketIndex);
                        break;
                    }
                }
            }
        }

        // Loop through the set and perform computation
        Set set = bracketsSet.entrySet();
        Iterator i = set.iterator();
        // Display elements
        int count = 0;
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            int openBracketPosition = (int) me.getKey();
            int closeBracketPosition = (int) me.getValue() - count;

            // if ( 1 + 3 + 3 ) then direct send to do compute
            if (closeBracketPosition - openBracketPosition != 4) {
                numberAndOperatorsList.remove(closeBracketPosition);
                numberAndOperatorsList.remove(openBracketPosition);
                List subList = new ArrayList(numberAndOperatorsList.subList(openBracketPosition, closeBracketPosition - 1));
                computeWithNoOpenParenthesis(subList, numberAndOperatorsList);

            } else {
                Double leftValue = Double.valueOf(numberAndOperatorsList.get(openBracketPosition + 1));
                String operand = numberAndOperatorsList.get(openBracketPosition + 2);
                Double rightValue = Double.valueOf(numberAndOperatorsList.get(closeBracketPosition - 1));
                result = calculateWithOperands(operand, leftValue, rightValue);

                // After compute, remove and replace the result with the compute value set
                // eg:  ( 1 + 3 ) - 1 -> replace the result 4 with ( 1 + 3 ), and it becomes 4 - 1

                numberAndOperatorsList.remove(closeBracketPosition);
                numberAndOperatorsList.remove(openBracketPosition + 3);
                numberAndOperatorsList.remove(openBracketPosition + 2);
                numberAndOperatorsList.remove(openBracketPosition + 1);
                numberAndOperatorsList.remove(openBracketPosition);
                numberAndOperatorsList.add(openBracketPosition, String.valueOf(result));
                count += 4;
            }
        }

        // if no open and close parenthesis at all, it will come to here and perform the compute logic
        List<String> duplicateList = new ArrayList<>(numberAndOperatorsList);
        result = computeWithNoOpenParenthesis(duplicateList, numberAndOperatorsList);
        return result;
    }

    public static double computeWithNoOpenParenthesis(List<String> str, List<String> numberAndOperatorsList) {
        double finalResult = 0;
        while (str.contains("*") || str.contains("/") || str.contains("+") || str.contains("-")) {
            int position = 0;
            int strPosition = 0;
            double result = 0;
            // Compute with multiply and divide first before sum and deduct
            if (numberAndOperatorsList.contains("*")) {
                position = numberAndOperatorsList.indexOf("*");
                strPosition = str.indexOf("*");
            } else if (numberAndOperatorsList.contains("/")) {
                position = numberAndOperatorsList.indexOf("/");
                strPosition = str.indexOf("/");
            } else if (numberAndOperatorsList.contains("+")) {
                position = numberAndOperatorsList.indexOf("+");
                strPosition = str.indexOf("+");
            } else if (numberAndOperatorsList.contains("-")) {
                position = numberAndOperatorsList.indexOf("-");
                strPosition = str.indexOf("-");
            }
            Double passIn = result == 0 ? Double.valueOf(str.get(strPosition + 1)) : result;

            result = calculateWithOperands(str.get(strPosition),
                    Double.valueOf(str.get(strPosition - 1)), passIn);
            numberAndOperatorsList.remove(position + 1);
            numberAndOperatorsList.remove(position);
            numberAndOperatorsList.add(position, String.valueOf(result));
            numberAndOperatorsList.remove(position - 1);

            str.remove(strPosition + 1);
            str.remove(strPosition);
            str.add(strPosition, String.valueOf(result));
            str.remove(strPosition - 1);
            finalResult = result;
        }

        return finalResult;
    }

    public static double calculateWithOperands(String operand, double leftValue, double rightValue) {
        double result = 0;
        switch (operand) {
            case "*":
                result = leftValue * rightValue;
                break;
            case "/":
                result = leftValue / rightValue;
                break;
            case "+":
                result = leftValue + rightValue;
                break;
            case "-":
                result = leftValue - rightValue;
                break;

        }
        return result;
    }
}
